import express from "express"
import { clients, sections } from "../app.js";
import { mqttClient } from "../mqtt.js";

export const controlsRouter = express.Router();

controlsRouter.post('/togglemonitormode', async (req, res) => {
  if (sections.length === 0) return res.status(400).json({ message: 'NO NODES' });
  for (let clientId of sections) {
    const client = clients.get(clientId)
    if (client === undefined) continue;
    if (client.isDayTime) continue;
    await mqttClient.publishAsync(clientId, "override");
    client.overridden = !client.overridden
  }
  res.status(200).json({ messsage: 'toggled' });
})

controlsRouter.post('/toggletime', async (req, res) => {
  if (sections.length === 0) return res.status(400).json({ message: 'NO NODES' });
  for (let clientId of sections) {
    const client = clients.get(clientId)
    console.log(client)
    if (client === undefined) continue;
    if (!client.overridden) await mqttClient.publishAsync(clientId, "override");
    if (!client.overridden) client.overridden = true
    if (client.isDayTime && client.overridden) {
      await mqttClient.publishAsync(clientId, "override")
      client.overridden = false;
    }
    client.isDayTime = !client.isDayTime;
  }
  res.status(200).json({ messsage: 'toggled' });
})


controlsRouter.post('/sectionhigh', async (req, res) => {
  const { clientId, laneName } = req.body;
  if (clientId === undefined || laneName === undefined) {
    return res.status(400).json({ message: 'CLIENTID OR LANENAME IS UNDEFINED' });
  }
  if (!sections.includes(clientId)) return res.status(400).json({ message: 'CLIENTID IS NOT A VALID SECTION' });
  const client = clients.get(clientId);
  if (client === undefined) return res.status(400).json({ message: 'INTERNAL SERVER ERROR' });
  if (!client.overridden) return res.status(400).json({ message: 'NOT IN CONTORL MODE' });
  await mqttClient.publishAsync(clientId, `${laneName}On`);
  return res.status(200).json({ message: 'turned on' });
})

controlsRouter.post('/sectionlow', async (req, res) => {
  const { clientId, laneName } = req.body;
  if (clientId === undefined || laneName === undefined) {
    return res.status(400).json({ message: 'CLIENTID OR LANEnAME IS UNDEFINED' });
  }
  if (!sections.includes(clientId)) return res.status(400).json({ message: 'CLIENTID IS NOT A VALID SECTION' });
  const client = clients.get(clientId);
  if (client === undefined) return res.status(400).json({ message: 'INTERNAL SERVER ERROR' });
  if (!client.overridden) return res.status(400).json({ message: 'NOT IN CONTORL MODE' });
  await mqttClient.publishAsync(clientId, `${laneName}Off`);
  return res.status(200).json({ message: 'turned on' });
})

controlsRouter.post('/lanehigh', async (req, res) => {
  const laneName = req.body.laneName;
  let err = false;
  if (laneName === undefined || laneName === "none") return res.status(400).json({ message: 'LANE NAME IS UNDEFINED' });
  for (let clientId of sections) {
    const client = clients.get(clientId);
    if (client === undefined) continue;
    if (!client.overridden) {
      err = true
      break
    };
  }
  if (err) return res.status(400).json({ message: 'NOT IN CONTORL MODE' });
  for (let clientId of sections) {
    const client = clients.get(clientId);
    if (client === undefined) continue;
    await mqttClient.publishAsync(clientId, `${laneName}On`);
  }
  res.status(200).json({ message: 'turned on' });
})

controlsRouter.post('/lanelow', async (req, res) => {
  const laneName = req.body.laneName;
  let err = false;
  if (laneName === undefined || laneName === "none") return res.status(400).json({ message: 'LANE NAME IS UNDEFINED' });
  for (let clientId of sections) {
    const client = clients.get(clientId);
    if (client === undefined) continue;
    if (!client.overridden) {
      err = true;
      break;
    };
  }
  if (err) return res.status(400).json({ message: 'NOT IN CONTORL MODE' });
  for (let clientId of sections) {
    const client = clients.get(clientId);
    if (client === undefined) continue;
    await mqttClient.publishAsync(clientId, `${laneName}Off`);
  }
  res.status(200).json({ message: 'turned on' });
})


controlsRouter.post('/roadhigh', async (req, res) => {
  if (sections.length === 0) return res.status(400).json({ message: 'NO NODES' });
  let err = false;
  for (let clientId of sections) {
    const client = clients.get(clientId);
    if (client === undefined) continue;
    if (!client.overridden) {
      err = true;
      break;
    }
  }
  if (err) return res.status(400).json({ message: 'NOT IN CONTORL MODE' });
  for (let clientId of sections) {
    const client = clients.get(clientId);
    if (client === undefined) continue;
    await mqttClient.publishAsync(clientId, `laneAOn`);
    await mqttClient.publishAsync(clientId, `laneBOn`);
  }
  res.status(200).json({ message: 'turned on' });
})


controlsRouter.post('/roadlow', async (req, res) => {
  if (sections.length === 0) return res.status(400).json({ message: 'NO NODES' });
  let err = false;
  for (let clientId of sections) {
    const client = clients.get(clientId);
    if (client === undefined) continue;
    if (!client.overridden) {
      err = true;
      break;
    }
  }
  if (err) return res.status(400).json({ message: 'NOT IN CONTORL MODE' });
  for (let clientId of sections) {
    const client = clients.get(clientId);
    if (client === undefined) continue;
    await mqttClient.publishAsync(clientId, `laneAOff`);
    await mqttClient.publishAsync(clientId, `laneBOff`);
  }
  res.status(200).json({ message: 'turned on' });
})


controlsRouter.post('/schedule', async (req, res) => {
  if (sections.length === 0) return res.status(400).json({ message: 'NO NODES' });
  const time = parseInt(req.body.timeInMin);
  if (isNaN(time)) return res.status(400).json({ message: 'TIME IS NOT A NUMBER' });
  if (time < 0) return res.status(400).json({ message: 'TIME IS NEGATIVE' });
  if (time === undefined) return res.status(400).json({ message: 'TIME IS UNDEFINED' });
  setTimeout(async () => {
    for (let clientId of sections) {
      const client = clients.get(clientId);
      if (client === undefined) continue;
      if (!client.overridden) await mqttClient.publishAsync(clientId, "override");
      if (!client.overridden) client.overridden = true;
      await mqttClient.publishAsync(clientId, "laneAOn");
      await mqttClient.publishAsync(clientId, "laneBOn");
    }
  }, time * 1000)
  res.status(200).json({ message: 'scheduled' });
})

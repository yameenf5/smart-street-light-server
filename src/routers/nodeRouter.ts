import express from 'express';
import { clients, sections } from '../app.js';
import { mqttClient, } from '../mqtt.js';

export const nodeRouter = express.Router();

nodeRouter.get('/', (req, res) => {
  res.json({ clients: [...clients], section: sections });
})

nodeRouter.post('/ping',async (req, res) => {
  const clientId = req.body.clientId;
  if (clientId === undefined) return res.status(404).json({ message: 'invalid client id' });
  await mqttClient.publishAsync(clientId, "pingSection");
  res.status(200).json({ message: 'ping send' });
})

nodeRouter.post('/delete', (req, res) => {
  const clientId = req.body.clientId;
  if (clientId === undefined) return res.status(404).json({ message: 'invalid client id' });
  clients.delete(clientId);
  const clientIdx = sections.indexOf(clientId);
  if (clientIdx !== -1) sections.splice(clientIdx, 1);
})

nodeRouter.post('/switch', (req, res) => {
  const clientId = req.body.clientId;
  const currentIdx = req.body.currentPosition - 1;
  const nextIdx = req.body.newPosition - 1;
  if (clientId === undefined || currentIdx === undefined || nextIdx === undefined) return res.status(404).json({ message: 'invalid client id' });
  if (currentIdx < 0 || currentIdx >= sections.length) return res.status(404).json({ message: 'invalid current position' });
  if (nextIdx < 0 || nextIdx >= sections.length) return res.status(404).json({ message: 'invalid next position' });
  if (currentIdx === nextIdx) return res.status(404).json({ message: 'current position and next position cannot be the same' });
  arrangeSections(sections, currentIdx, nextIdx);
  return res.json({ message: 'switched' });
})

function arrangeSections(sections: string[], currentIdx: number, nextIdx: number) {
  const client = sections[currentIdx];
  sections.splice(currentIdx, 1);
  sections.splice(nextIdx, 0, client);
}


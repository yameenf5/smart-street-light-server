import express from "express"
import { registerHandler, mqttClient, handlers } from "./mqtt.js"
import cors from "cors"
import { nodeRouter } from "./routers/nodeRouter.js"
import { controlsRouter } from "./routers/controlRouter.js"

const app = express()
const PORT = process.env.PORT || 3000
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())
app.use("/nodes", nodeRouter)
app.use("/controls", controlsRouter)

export const clients = new Map<string, { overridden: boolean, countLaneA: number, countLaneB: number, isDayTime: boolean }>
export const sections: string[] = [];

mqttClient.subscribe("connections");

registerHandler({ topic: "connections" }, async (_, message) => {
  const clientId = message.toString();
  clients.set(clientId, { overridden: false, countLaneA: 0, countLaneB: 0, isDayTime: false });
  if (!sections.includes(clientId)) {
    sections.push(clientId);
  }
  mqttClient.subscribe(clientId);
})

registerHandler({ topic: "client-*", message: "LaneACount" }, async (topic, message) => {
  const clientId = clients.get(topic);
  if (clientId === undefined) return;
  clientId.countLaneA = clientId.countLaneA + 1;
  const sectionIdx = sections.indexOf(topic);
  if (sectionIdx <= 0) return;
  const prevSectionIdx = sectionIdx - 1;
  const previousClient = clients.get(sections[prevSectionIdx]);
  console.log("previousClient is ", sections[prevSectionIdx])
  if (previousClient === undefined) return;
  if (previousClient.countLaneA >= 1) previousClient.countLaneA = previousClient.countLaneA - 1;
  if (previousClient.countLaneA <= 0) {
    await mqttClient.publishAsync(sections[prevSectionIdx], "laneAReset");
  }
})

registerHandler({ topic: "client-*", message: "LaneBCount" }, async (topic, message) => {
  const clientId = clients.get(topic);
  if (clientId === undefined) return;
  clientId.countLaneB = clientId.countLaneB + 1;
  const sectionIdx = sections.indexOf(topic);
  if (sectionIdx < 0 || sectionIdx === (sections.length - 1)) return;
  const nextSectionIdx = sectionIdx + 1;
  const nextClient = clients.get(sections[nextSectionIdx]);
  console.log("nextClient is ", sections[nextSectionIdx])
  if (nextClient === undefined) return;
  if (nextClient.countLaneB >= 1) nextClient.countLaneB = nextClient.countLaneB - 1;
  if (nextClient.countLaneB <= 0) {
    await mqttClient.publishAsync(sections[nextSectionIdx], "laneBReset");
  }
})

registerHandler({ topic: "client-*", message: "LaneAResetCount" }, async (topic, msg) => {
  const client = clients.get(topic);
  if (client === undefined) return;
  client.countLaneA = 0;
});

registerHandler({ topic: "client-*", message: "LaneBResetCount" }, async (topic, msg) => {
  const client = clients.get(topic);
  if (client === undefined) return;
  client.countLaneB = 0;
});


setInterval(() => {
  console.log("clients are ", clients)
  console.log("sections are ", sections)
  console.log("handlers are", handlers)
}, 3000);


app.listen(PORT, () => { console.log("app is running on port 3000") })


import { connectAsync } from "mqtt";

export const mqttClient = await connectAsync(
  {
    protocol: "mqtts",
    host: "451d34810c1c4f4bb17e6560fb1a6fae.s1.eu.hivemq.cloud",
    username: "yameen",
    password: "Yameen123", port: 8883, clientId: "client-123"
  });

export const handlers: Map<string, Map<string, (topic: string, message: string) => void>> = new Map();
const topicCallbackPlaceholder = "_topic_callback";

export function registerHandler({ topic, message }: { topic: string, message?: string }, cb: (topic: string, msg: string) => void) {
  if (!handlers.has(topic)) handlers.set(topic, new Map());
  const topicValue = handlers.get(topic);
  if (message === undefined) {
    if (topicValue) topicValue.set(topicCallbackPlaceholder, cb);
    return;
  }
  if (topicValue) topicValue.set(message, cb);
  return;
}

export function unRegisterHandler({ topic, message }: { topic: string, message?: string }) {
  if (!handlers.has(topic)) return;
  if (message === undefined) {
    handlers.delete(topic);
    return;
  }
  const topicValue = handlers.get(topic);
  if(topicValue) topicValue.delete(message);
  return;
}

function getPatternsFromStrings(strs: string[]) {
  return strs.filter((str) => str.charAt(str.length - 1) === "*").map((pattern) => pattern.substring(0, pattern.length - 1))
}

function getFristMatchingPattern(patterns: string[], str: string) {
  const ret = patterns.find(pattern => str.indexOf(pattern) === 0);
  if (ret) return (ret + "*");
  return undefined
}

mqttClient.on("message", (topic, message) => {
  let topicOrPattern = topic;
  if (!handlers.has(topic)) {
    const patterns = getPatternsFromStrings(Array.from(handlers.keys()));
    topicOrPattern = getFristMatchingPattern(patterns, topic) || topic;
  }

  const topicValue = handlers.get(topicOrPattern);
  if(topicValue === undefined) return;
  const topicHandler = topicValue.get(topicCallbackPlaceholder);
  if (topicHandler) topicHandler(topic, message.toString());

  const msgValue = handlers.get(topicOrPattern);
  if(msgValue === undefined) return;
  const msgHandler = msgValue.get(message.toString());
  if (msgHandler) return msgHandler(topic, message.toString());

  const msgHandlers = handlers.get(topicOrPattern);
  if (msgHandlers === undefined) return;

  const patterns = getPatternsFromStrings(Array.from(msgHandlers.keys()));
  const matchedPattern = getFristMatchingPattern(patterns, message.toString());
  if (!matchedPattern) return;
  const handler = msgHandlers.get(matchedPattern);
  if (handler)
    handler(topic, message.toString())
})
